package com.lib.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication		
@ComponentScan("com.lib.*")

public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);		
		
		// Metrics metric = Metrics.getInstance("masker-1", "runners");
		// metric.setStartTime();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.incrementAndUpdate();
		// metric.setEndTime();
		//
		// Metrics metric2 = Metrics.getInstance("masker-3", "secured");
		//
		// metric2.setStartTime();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.incrementAndUpdate();
		// metric2.setEndTime();
		}
}