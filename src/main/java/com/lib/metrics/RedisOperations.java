package com.lib.metrics;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisDataException;

@Component
@EnableScheduling
public class RedisOperations {

	private static Jedis jedis;

	public static final String SERVICE_ID = "serviceId";
	public static final String COUNTER = "counter";
	public static final String TOTAL_PROCESS_TIME = "totalProcessTime";
	public static final String LAST_PROCESS_TIME = "lastProcessTime";
	
	
	@Autowired
	@SuppressWarnings("static-access")
	private RedisOperations(Jedis jedis) {
		this.jedis = jedis;
	}
	/**
	 * Iterates through all metrics and stores them to Redis
	 * (Map containes instances of ServiceMetic) 
	 */
	@SuppressWarnings({"unchecked", "resource", "rawtypes"})
	@Scheduled(fixedRate = 5000)
	public static void updateMetricsToRedis() throws JedisDataException{
		String hsetName;
		String counterName;
		String channelName;
		
		Iterator instances= Metrics.instances.entrySet().iterator();
		while(instances.hasNext()) {
			Map.Entry<String,Metrics> pair = (Map.Entry)instances.next();
			counterName = pair.getValue().getCounterName();
			channelName = pair.getValue().getChannelName();
			hsetName = counterName+":"+channelName;
			
			//System.out.println("EXISTS hash set name: " + counterName);
			jedis.hset(hsetName, SERVICE_ID, counterName);
			jedis.hset(hsetName, COUNTER, String.valueOf(Metrics.getInstance(counterName, channelName).get()));
			jedis.hset(hsetName, TOTAL_PROCESS_TIME, String.valueOf(TimeUnit.MILLISECONDS.convert(Metrics.getInstance(counterName, channelName).getTotalProcessTime(), TimeUnit.NANOSECONDS) + " milli seconds"));
			jedis.hset(hsetName, LAST_PROCESS_TIME, String.valueOf(TimeUnit.MILLISECONDS.convert(Metrics.getInstance(counterName, channelName).getLastProcessTime(), TimeUnit.NANOSECONDS) + " milli seconds"));
		}
	}

	/**
	 * Returns the metrics of the provided params that are stored to Redis.
	 * 
	 * @param counterName
	 * @param channelName
	 * @return
	 */
	@SuppressWarnings("resource")
	public static JSONObject getMetricsFromRedis(String counterName, String channelName) {

		String hashSetName = counterName + ":" + channelName;
		if (jedis.exists(hashSetName)) {
			return new JSONObject(jedis.hgetAll(hashSetName));
		} else
			return new JSONObject("does not exist");
	}
}
