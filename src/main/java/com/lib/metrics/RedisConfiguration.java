package com.lib.metrics;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;

@Configuration
@PropertySource("classpath:application.properties")
public class RedisConfiguration {

	@Value("${pass}")
	public String pass;

	
	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	@Bean
	public JedisShardInfo info() {
		JedisShardInfo info = new JedisShardInfo("localhost", 6379);
		info.setPassword(getPass());
		return info;
	}
	
	@Bean
	public Jedis jedis() {
		Jedis jedis = new Jedis(info());
		return jedis;
	}
}
