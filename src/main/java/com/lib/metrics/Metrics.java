package com.lib.metrics;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class Metrics {
	
	public final static String CURRENT_SERVICE_ID = UUID.randomUUID().toString();
	private long startTime;
	private long endTime;
	private long totalProcessTime = 0;
	private long lastProcessTime = 0;
	
	/**
	 * Name of message channel, which we want to measure
	 */
	private String channelName;
	
	/**
	 * Name of counter, which we want to measure, alias for UUID (the name of the service eg. makser-1)
	 */
	private String counterName;
	
	/**
	 * The counter for measured messages. 
	 */
	private AtomicLong counter = new AtomicLong(0);

	/**
	 * List of counter, which are currently in use
	 * 
	 * Changed this to public in order to use it in RedisOperations.updateToRedis() iteration.
	 * @Scheduled
	 * updateMetricsToRedis
	 */
	public static Map<String, Metrics> instances = new HashMap<String, Metrics>();
	
	
	public Metrics() {}
	private Metrics(final String counterName, final String channelName) {
		this.counterName = counterName;
		this.channelName = channelName;
	}

	/**
	 * Return counter for channel name
	 * 
	 * @param counterName
	 * @param channelName
	 * @return
	 */
	public static Metrics getInstance(final String counterName, final String channelName) {
		final String key = getInstanceName(counterName, channelName);
		if(!instances.containsKey(key)) {
			System.out.println(counterName+":"+channelName);
			
			Metrics newMetric = new Metrics(counterName, channelName);
			instances.put(key, newMetric);
		}
		return instances.get(key);
	}

	/**
	 * Build specific key for each instance
	 * @param counterName
	 * @param channelName
	 * @return
	 */
	private static String getInstanceName(final String counterName, final String channelName) {
		return counterName + ":" + channelName;	
	}
	
	/**
	 * Increment and update counter to Metrics
	 * 
	 * @param counterName
	 * @param atomicCounter
	 */
	public void incrementAndUpdate() {
		this.counter.incrementAndGet();
		Metrics.getInstance(this.counterName, this.channelName).set(counter);
		
		System.out.println("*" + this.counterName + ":" +this.channelName+ ":" + Metrics.getInstance(counterName, channelName).get());
	}
	
	public AtomicLong get() {
		return this.counter;	
	}
	
	/**
	 * Set new values for counter.
	 * @param counter
	 */
	public void set(final AtomicLong counter) {
		this.counter.set(counter.get());
	}
	
	// Getters
	public String getCounterName() {
		return counterName;
	}
	
	public String getChannelName() {
		return channelName;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public long getEndTime() {
		return endTime;
	}
	
	public long getLastProcessTime() {
		return lastProcessTime;
	}
	
	public long getTotalProcessTime() {
		return totalProcessTime;
	}
	
	// Setters
	public void setStartTime() {
		startTime = System.nanoTime();
	}

	public void setEndTime() {
		endTime = System.nanoTime();
		setLastProcessTime(endTime - startTime);
		setTotalProcessTime(endTime - startTime);
	}
	
	public void setLastProcessTime(long processTime) {
		this.lastProcessTime = processTime;
	}
	
	public void setTotalProcessTime(long processTime) {
		this.totalProcessTime += processTime;
	}
}