package com.lib.metrics;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MetricsService {

	private static final String INIT_ERROR = "Reader: \nMetrics are not initialized.\nCheck if you have sent any events \nOr your request is not valid.";
	
	/**
	 * Returns the metrics stored to Redis
	 * 
	 * @param counterName The name of the counter metrics
	 * @param channelName The name of the channel that the counter measures
	 * @return
	 */
	@RequestMapping(value = "metrics", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String metrics(@RequestParam(value = "counter") String counterName, @RequestParam(value = "channel") String channelName) {

		try {
			System.out.println("\n\nReader metrics: \n"
					+ RedisOperations.getMetricsFromRedis(counterName, channelName).toString());

			return RedisOperations.getMetricsFromRedis(counterName, channelName).toString();
		} catch (NullPointerException e) {
			System.out.println(INIT_ERROR);
			return null;
		}
		
	}
}
